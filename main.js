// Exercise 1
function calcTotalPoint(firstPoint, secondPoint, thirdPoint) {
  return firstPoint + secondPoint + thirdPoint;
}

const X = "X";
const A = "A";
const B = "B";
const C = "C";
function plusAreaPoint(selArea) {
  switch (selArea) {
    case X:
      return 0;
    case A:
      return 2;
    case B:
      return 1;
    case C:
      return 0.5;
  }
}

const zero = "0";
const one = "1";
const two = "2";
const three = "3";
function plusStudentPoint(selStudent) {
  switch (selStudent) {
    case zero:
      return 0;
    case one:
      return 2.5;
    case two:
      return 1.5;
    case three:
      return 1;
  }
}

function classifyStudent() {
  var wishedPointValue = +document.getElementById("wishedPointInput").value;
  var firstPointValue = +document.getElementById("point1Input").value;
  var secondPointValue = +document.getElementById("point2Input").value;
  var thirdPointValue = +document.getElementById("point3Input").value;

  var areaValue = document.getElementById("selArea").value;
  var studentValue = document.getElementById("selStudent").value;

  var points = calcTotalPoint(
    firstPointValue,
    secondPointValue,
    thirdPointValue
  );
  var areaPoint = plusAreaPoint(areaValue);
  var studentPoint = plusStudentPoint(studentValue);

  var totalPoint = points + areaPoint + studentPoint;

  if (firstPointValue == 0 || secondPointValue == 0 || thirdPointValue == 0) {
    document.getElementById(
      "result1"
    ).innerHTML = `Bạn đã rớt vì bạn đã đạt điểm 0`;
  } else if (totalPoint < wishedPointValue) {
    document.getElementById(
      "result1"
    ).innerHTML = `Bạn đã rớt với tổng điểm là ${totalPoint} `;
  } else {
    document.getElementById(
      "result1"
    ).innerHTML = `Bạn đã đậu với tổng điểm là ${totalPoint}`;
  }
}

// Exerccise 2
function calcFirst50kW(kWValue) {
  if (kWValue <= 50) {
    return 500 * kWValue;
  } else {
    return 500 * 50;
  }
}

function calcNext50kW(kWValue) {
  if (50 < kWValue && kWValue <= 100) {
    return 650 * (kWValue - 50);
  } else {
    return 650 * 50;
  }
}

function calcNext100kW(kWValue) {
  if (100 < kWValue && kWValue <= 200) {
    return 850 * (kWValue - 100);
  } else {
    return 850 * 100;
  }
}

function calcNext150kW(kWValue) {
  if (200 < kWValue && kWValue <= 350) {
    return 1100 * (kWValue - 200);
  } else {
    return 1100 * 150;
  }
}

function calcOtherkW(kWValue) {
  if (kWValue > 350) {
    return 1300 * (kWValue - 350);
  }
}

function calcElecBill() {
  var nameValue = document.getElementById("elecBillNameInput").value;
  var kWValue = +document.getElementById("kWInput").value;

  var first50kW = calcFirst50kW(kWValue);
  var next50kW = calcNext50kW(kWValue);
  var next100kW = calcNext100kW(kWValue);
  var next150kW = calcNext150kW(kWValue);
  var otherkW = calcOtherkW(kWValue);

  var elecBill = 0;

  if (kWValue <= 50) {
    elecBill = first50kW;
  } else if (50 < kWValue && kWValue <= 100) {
    elecBill = first50kW + next50kW;
  } else if (100 < kWValue && kWValue <= 200) {
    elecBill = first50kW + next50kW + next100kW;
  } else if (200 < kWValue && kWValue <= 350) {
    elecBill = first50kW + next50kW + next100kW + next150kW;
  } else {
    elecBill = first50kW + next50kW + next100kW + next150kW + otherkW;
  }

  var formatter = new Intl.NumberFormat("vn-VN", {
    style: "currency",
    currency: "VND",
  });

  document.getElementById(
    "result2"
  ).innerHTML = `Họ và tên: ${nameValue}, tiền điện: ${formatter.format(
    elecBill
  )}`;
}

// Exercise 3
function calcPaymentTax(salary, peopleRely) {
  var personalTax = salary - 4e6 - peopleRely * 1.6e6;
  return personalTax;
}

function calcTax() {
  var nameValue = document.getElementById("taxBillNameInput").value;

  var salaryValue = +document.getElementById("salaryInput").value;
  var peopleRelyValue = +document.getElementById("peopleRelyInput").value;

  var personalTax = calcPaymentTax(salaryValue, peopleRelyValue);

  var totalTax = 0;

  if (personalTax <= 4e6) {
    alert`Số tiền thu nhập không hợp lệ`;
  } else if (4e6 < personalTax && personalTax <= 60e6) {
    totalTax = 0.05 * personalTax;
  } else if (60e6 < personalTax && personalTax <= 120e6) {
    totalTax = 0.1 * personalTax;
  } else if (120e6 < personalTax && personalTax <= 210e6) {
    totalTax = 0.15 * personalTax;
  } else if (210e6 < personalTax && personalTax <= 384e6) {
    totalTax = 0.2 * personalTax;
  } else if (384e6 < personalTax && personalTax <= 624e6) {
    totalTax = 0.25 * personalTax;
  } else if (624e6 < personalTax && personalTax <= 960e6) {
    totalTax = 0.3 * personalTax;
  } else {
    totalTax = 0.35 * personalTax;
  }

  var formatter = new Intl.NumberFormat("vn-VN", {
    style: "currency",
    currency: "VND",
  });

  document.getElementById(
    "result3"
  ).innerHTML = `Họ và tên: ${nameValue}, Tiền thuế thu nhập cá nhân: ${formatter.format(
    totalTax
  )}`;
}

// Exercise 4
function calcFamilyBill(advancedChanel) {
  return 25 + advancedChanel * 7.5;
}

function calcCompanyBill(advancedChanel, connectedCable) {
  if (connectedCable <= 10) {
    return 15 + 75 + 50 * advancedChanel;
  } else {
    return 15 + 75 + 50 * advancedChanel + (connectedCable - 10) * 5;
  }
}

function showInput() {
  var showValue = document.getElementById("selBussiness").value;
  document.getElementById("hiddenBlock").style.display =
    "companyOption" == showValue ? "block" : "none";
}

function calcCableBill() {
  var bussinessValue = document.getElementById("selBussiness").value;
  var customerID = document.getElementById("customerIDInput").value;

  var advancedChanel = +document.getElementById("advancedChanelInput").value;
  var connectedCable = +document.getElementById("connectedCableInput").value;

  var familyBill = calcFamilyBill(advancedChanel);
  var companyBill = calcCompanyBill(advancedChanel, connectedCable);

  var totalCableBill = 0;

  if (bussinessValue == "familyOption") {
    totalCableBill = familyBill;
  } else {
    totalCableBill = companyBill;
  }

  var formatter = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
  });

  document.getElementById(
    "result4"
  ).innerHTML = `Mã khách hàng ${customerID}, tiền cáp: ${formatter.format(
    totalCableBill
  )}`;
}
